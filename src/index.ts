import { tasks } from './tasks';
import { PLUGIN_CONFIG_ENV_KEY, SnapshotVerifyConfig, initConfig } from './config';
import initServer from './server';
import { addMatchSnapshotCommand } from './commands';

/**
 * Initializes the plugin by registering tasks for `matchesSnapshot` and makes config accessible via Cypress env
 * @param on Method to register tasks
 * @param globalConfig Global Cypress config options
 */
function initPlugin(on: Cypress.PluginEvents, globalConfig: Cypress.PluginConfigOptions) {
  // Load some or config values from env Cypress config env before replacing env value with merged config
  const envConfig: Partial<SnapshotVerifyConfig> =
    (globalConfig.env[PLUGIN_CONFIG_ENV_KEY] as Partial<SnapshotVerifyConfig>) || {};

  // initConfig will extract all the fields we care about from the global config and merge it with our plugin options
  const config = initConfig(envConfig);
  globalConfig.env[PLUGIN_CONFIG_ENV_KEY] = config;

  if (config.serverEnabled) {
    initServer(config);
  }

  on('before:browser:launch', (browser, launchOptions) => {
    const args = Array.isArray(launchOptions) ? launchOptions : launchOptions.args;

    if (browser.name === 'chrome') {
      args.push('--font-render-hinting=medium', '--enable-font-antialiasing', '--disable-gpu');
    }

    return launchOptions;
  });

  on('task', tasks);

  return config;
}

export { initPlugin, addMatchSnapshotCommand };
