import { SNAPSHOT_DEFAULT_DIR } from './constants';

const PLUGIN_CONFIG_ENV_KEY = 'cypress-snapshot-verify';

export interface SnapshotVerifyConfig {
  passNewSnapshots: boolean;
  updateFailingSnapshots: boolean;
  errorOnMatchingSnapshotNames: boolean;
  snapshotsFolder: string;
  maxDiffPercentage: number;
  serverEnabled?: boolean;
  serverHost: string;
  serverPort: number;
}

const DEFAULT_CONFIG: SnapshotVerifyConfig = {
  passNewSnapshots: true,
  updateFailingSnapshots: false,
  errorOnMatchingSnapshotNames: true,
  snapshotsFolder: SNAPSHOT_DEFAULT_DIR,
  maxDiffPercentage: 0.0,
  serverEnabled: true,
  serverHost: 'localhost',
  serverPort: 2121,
};

let config: SnapshotVerifyConfig | undefined = undefined;

function initConfig(initialConfig?: Partial<SnapshotVerifyConfig>): SnapshotVerifyConfig {
  config = { ...DEFAULT_CONFIG, ...initialConfig };
  return config;
}

function getConfig(): SnapshotVerifyConfig {
  return config || DEFAULT_CONFIG;
}

export { PLUGIN_CONFIG_ENV_KEY, initConfig, getConfig };
