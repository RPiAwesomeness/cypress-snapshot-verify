const NO_LOG = { log: false } as const;

const SNAPSHOT_DEFAULT_DIR = 'cypress/snapshots';

const EVENT_LOAD_IMAGE = 'cypress-snapshot-verify:loadImage';

const COMMAND_NAME = 'matchesSnapshot';

const MODAL_ELEM_ID = 'cypress-snapshot-verify-modal';
const MODAL_ROOT_SELECTOR = `#${MODAL_ELEM_ID}`;

const TASK_LOAD_FILE = 'cypress-snapshot-verify:loadFile';
const TASK_MATCH_SCREENSHOT = 'cypress-snapshot-verify:matchScreenshot';
const TASK_SAVE_IMAGE = 'cypress-snapshot-verify:saveImage';
const TASK_UPDATE_BASE = 'cypress-snapshot-verify:updateBaseScreenshot';

export {
  NO_LOG,
  SNAPSHOT_DEFAULT_DIR,
  EVENT_LOAD_IMAGE,
  COMMAND_NAME,
  MODAL_ELEM_ID,
  MODAL_ROOT_SELECTOR,
  TASK_LOAD_FILE,
  TASK_MATCH_SCREENSHOT,
  TASK_SAVE_IMAGE,
  TASK_UPDATE_BASE,
};
