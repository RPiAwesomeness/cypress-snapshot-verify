import { createServer } from 'http';
import { copyFileSync } from 'fs';
import { SnapshotVerifyConfig } from './config';

const CORS_headers = {
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': 'Content-Type, Accept',
  'Access-Control-Allow-Methods': 'OPTIONS, POST',
};

interface ScreenshotPathData {
  expected: string;
  actual: string;
}

function isScreenshotPaths(body: unknown): body is ScreenshotPathData {
  return body instanceof Object && 'expected' in body && 'actual' in body;
}

export default function initServer({ serverHost: host, serverPort: port }: SnapshotVerifyConfig) {
  console.log('Initializing snapshot server...');

  const server = createServer((req, res) => {
    if (req.method === 'OPTIONS') {
      res.writeHead(200, { Allow: 'OPTIONS, POST', ...CORS_headers });
      res.end();
      return;
    }
    if (req.method !== 'POST') {
      res.writeHead(405, CORS_headers);
      res.end();
      return;
    }

    let body = '';
    req.on('data', (chunk) => {
      body += chunk;
    });
    req.on('end', () => {
      let responseContent: string | object = '';
      try {
        const data: unknown = JSON.parse(body);
        if (!isScreenshotPaths(data)) {
          res.writeHead(400, CORS_headers);
          responseContent = 'Invalid data, missing required keys';
          return;
        }

        // TODO: Add duplicate check here for existing snapshot files per `errorOnMatchingSnapshotNames` config value
        // Copy updated screenshot over original expected
        copyFileSync(data.actual, data.expected);

        res.writeHead(200, { 'Content-Type': 'application/json', ...CORS_headers });
        responseContent = { success: true, message: `Updated screenshot at ${data.expected}` };
      } catch (ex) {
        responseContent = (ex as Error).message || 'server failed to complete screenshot copy request';
      } finally {
        res.end(JSON.stringify(responseContent instanceof Object ? responseContent : { error: responseContent }));
      }
    });
  });

  server.listen(port, host);
}
