import { existsSync, statSync, readFileSync } from 'fs';
import { join } from 'path';
import { PNG } from 'pngjs';

import sanitizeFilename from 'sanitize-filename';
import { TaskData } from '../utils';

interface PathWithDir {
  dir: string;
  fullPath: string;
}

export interface PNGWithDetails {
  image: PNG;
  width: number;
  height: number;
  path: string;
}

function getSnapshotPaths({ spec, snapshotTitle, snapshotFolder, isHeadless }: TaskData): PathWithDir {
  const dir = join(process.cwd(), snapshotFolder, isHeadless ? spec.name : '');
  const filename = sanitizeFilename(`${snapshotTitle}.png`);
  return { dir, fullPath: join(dir, filename) };
}

function getImage(filename: string): PNG | null {
  const exists = existsSync(filename);
  if (!exists) return null;

  const size = statSync(filename).size;
  if (size <= 0) return null;

  return PNG.sync.read(readFileSync(filename));
}

function getImageWithMetadata(filename: string): PNGWithDetails | null {
  const img = getImage(filename);
  return img
    ? {
        image: img,
        width: img.width,
        height: img.height,
        path: filename,
      }
    : null;
}

export { getImageWithMetadata, getSnapshotPaths };
