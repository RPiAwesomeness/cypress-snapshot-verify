import { join, resolve } from 'path';
import { existsSync, readFileSync } from 'fs';

const SCRIPT_PATH = './assets/modal.js';
const CSS_PATH = './assets/modal.css';
const MODAL_PATH = './assets/modal.html';
type VALID_PATHS = typeof SCRIPT_PATH | typeof CSS_PATH | typeof MODAL_PATH;

function getFile(filename: VALID_PATHS): string {
  const fullPath = filename.match(/^\.\//) ? join(resolve(__dirname, '../../', filename)) : require.resolve(filename);

  if (!existsSync(fullPath)) {
    throw new Error(`File '${filename}' cannot be found. Location: '${fullPath}'`);
  }

  return readFileSync(fullPath, { encoding: 'utf-8' });
}

export { getFile, SCRIPT_PATH, CSS_PATH, MODAL_PATH };
