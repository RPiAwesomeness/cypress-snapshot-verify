import { mkdirSync, existsSync, copyFileSync } from 'fs';
import { PNG } from 'pngjs';
import pixelmatch from 'pixelmatch';

import { PNGWithDetails, getImageWithMetadata, getSnapshotPaths } from './utils';
import { TaskData } from '../utils';
import { getConfig } from '../config';

export interface ImageDetails_B64 {
  b64: string;
  width: number;
  height: number;
  path?: string;
}

export type DiffDetails = ImageDetails_B64 & {
  diffPixelCount: number;
  diffPixelPercentage: number;
};

export interface SnapshotComparisonDetails {
  result: 'pass' | 'fail' | 'size' | 'created' | 'updated';
  actual: ImageDetails_B64 & { path: string };
}

type NoDiffResult = SnapshotComparisonDetails & {
  result: 'created' | 'updated';
};

type DiffResult = SnapshotComparisonDetails & {
  result: 'fail' | 'pass';
  expected: ImageDetails_B64 & { path: string };
  diff: DiffDetails;
};

type NoCompareResult = SnapshotComparisonDetails & {
  result: 'size';
  expected: ImageDetails_B64 & { path: string };
  diff: undefined;
};

export type SnapshotComparisonResult = NoDiffResult | NoCompareResult | DiffResult;

function makeB64ImageDetails(img: PNG, filterType = 2): ImageDetails_B64 {
  return {
    b64: PNG.sync.write(img, { filterType }).toString('base64'),
    width: img.width,
    height: img.height,
  };
}

function generateDiff(expected: PNG, actual: PNG): DiffDetails {
  const width = actual.width;
  const height = actual.height;
  const diffImg = new PNG({ height, width });

  const diffPixelCount = pixelmatch(actual.data, expected.data, diffImg.data, height, width, { threshold: 0.0 });
  const diffPixelPercentage = (diffPixelCount / (width * height)) * 100;
  return {
    ...makeB64ImageDetails(diffImg),
    diffPixelCount,
    diffPixelPercentage,
  };
}

function copyAndResolve(
  src: PNGWithDetails,
  dest: string,
  result: NoDiffResult['result'],
  throwOnExist: boolean,
): Promise<NoDiffResult> {
  if (throwOnExist && existsSync(dest)) {
    throw new Error(
      `snapshot at '${dest}' already exists. Set \`errorOnMatchingSnapshotNames\` to \`false\` to disable this check`,
    );
  }
  copyFileSync(src.path, dest);

  const pngBuff = PNG.sync.write(src.image, { filterType: 2 });
  return Promise.resolve({
    result,
    actual: {
      ...src,
      b64: pngBuff.toString('base64'),
    },
  });
}

async function matchSnapshot(details: TaskData): Promise<SnapshotComparisonResult> {
  const { passNewSnapshots, updateFailingSnapshots, errorOnMatchingSnapshotNames } = getConfig();

  // Load actual screenshot from disk - obviously have to have a screenshot to compare against
  if (!details.snapshotDetails) throw new Error('image details not returned from screenshot');

  const actualPath = details.snapshotDetails.path;
  const actual = getImageWithMetadata(actualPath);
  if (!actual) throw new Error('failed to load actual screenshot from disk');

  // Attempt to load expected screenshot (snapshot) from disk
  const { fullPath: expectedPath, dir: expectedDir } = getSnapshotPaths(details);
  const expected = getImageWithMetadata(expectedPath);

  // Ensure snapshots directory exists
  mkdirSync(expectedDir, { recursive: true });
  if (!expected) {
    if (!passNewSnapshots) {
      throw new Error('failed to load expected snapshot from disk (not automatically creating new snapshots)');
    }

    // Save actual image as base snapshot
    return copyAndResolve(actual, expectedPath, 'created', errorOnMatchingSnapshotNames);
  }

  // Can only perform diff if width/height are the same between both images
  if (actual.width !== expected.width || actual.height !== expected.height) {
    if (updateFailingSnapshots) {
      // Overwrite former base snapshot
      return copyAndResolve(actual, expectedPath, 'updated', errorOnMatchingSnapshotNames);
    }

    // Configured to not update base snapshots results in a size failure
    return Promise.resolve<NoCompareResult>({
      result: 'size',
      actual: {
        ...actual,
        b64: PNG.sync.write(actual.image, { filterType: 2 }).toString('base64'),
      },
      expected: {
        ...expected,
        b64: PNG.sync.write(expected.image, { filterType: 2 }).toString('base64'),
      },
      diff: undefined,
    });
  }

  // Image sizes match, go ahead with diff
  const diff = generateDiff(expected.image, actual.image);
  const diffResult: DiffResult = {
    result: 'pass',
    actual: {
      ...actual,
      b64: PNG.sync.write(actual.image, { filterType: 2 }).toString('base64'),
    },
    expected: {
      ...expected,
      b64: PNG.sync.write(expected.image, { filterType: 2 }).toString('base64'),
    },
    diff,
  };
  if (diff.diffPixelPercentage > details.maxDiffPercentage) {
    return Promise.resolve<DiffResult>({ ...diffResult, result: 'fail' });
  }

  return Promise.resolve<DiffResult>(diffResult);
}

export { matchSnapshot };
