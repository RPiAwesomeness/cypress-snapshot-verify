import { matchSnapshot } from './snapshot';
import { TASK_MATCH_SCREENSHOT, TASK_LOAD_FILE } from '../constants';
import { getFile } from './getFile';

const tasks = {
  [TASK_MATCH_SCREENSHOT]: matchSnapshot,
  [TASK_LOAD_FILE]: getFile,
};

export { tasks };
