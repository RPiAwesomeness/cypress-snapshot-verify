import { CypressScreenshotProps, TaskData, getTaskData } from '../utils';
import { NO_LOG, TASK_MATCH_SCREENSHOT } from '../constants';
import { SnapshotComparisonResult } from '../tasks/snapshot';
import { logMessage } from './log';

type SnapshotMatchResult = SnapshotComparisonResult & { subject: JQuery; taskData: TaskData };

function afterScreenshot(taskData: TaskData) {
  return (_: JQuery, props: CypressScreenshotProps) => {
    taskData.snapshotDetails = props;
  };
}

function matchesSnapshot(
  subject: JQuery,
  snapshotFolder: string,
  maxDiffPercentage: number,
  customTitle?: string,
  screenshotConfig: Partial<Cypress.ScreenshotOptions> = {},
) {
  const taskData = getTaskData(snapshotFolder, customTitle, maxDiffPercentage);

  // Run both our afterScreenshot and the specified one if present
  const afterScreenshotFn = afterScreenshot(taskData);
  if (screenshotConfig.onAfterScreenshot) {
    const callback = screenshotConfig.onAfterScreenshot;
    screenshotConfig.onAfterScreenshot = function (...args) {
      afterScreenshotFn.apply(this, args);
      callback.apply(this, args);
    };
  } else {
    screenshotConfig.onAfterScreenshot = afterScreenshotFn;
  }

  return cy
    .wrap(subject, NO_LOG)
    .screenshot(taskData.snapshotTitle, { ...screenshotConfig, ...NO_LOG })
    .then(() => cy.task<SnapshotComparisonResult>(TASK_MATCH_SCREENSHOT, taskData, NO_LOG))
    .then((result) => logMessage({ ...result, subject, taskData }));
}

export { matchesSnapshot };
export type { SnapshotMatchResult };
