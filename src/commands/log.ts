import { COMMAND_NAME } from '../constants';
import { ImageDetails_B64 } from '../tasks/snapshot';
import { SnapshotMatchResult } from './snapshot';

declare global {
  namespace Cypress {
    interface LogConfig {
      state: string;
    }
  }
}

const diffThresholdErrorMessage = (diffPixelPercentage: number, maxDiffPercentage = 0.0) =>
  `crossed diff threshold **${diffPixelPercentage.toFixed(2)}%** > ${maxDiffPercentage.toFixed(2)}%`;
const mismatchSizeErrorMessage = (expected: ImageDetails_B64, actual: ImageDetails_B64) =>
  `image size differs (expected ${expected.width}x${expected.height}, got ${actual.width}x${actual.height})`;

function generateLogMessage(result: SnapshotMatchResult, id: string, interactive: boolean): string {
  if (result.result === 'created') {
    return 'automatically passed - created initial snapshot';
  } else if (result.result === 'updated') {
    return 'automatically passed - updated existing snapshot';
  } else if (result.result === 'size') {
    const errMsg = mismatchSizeErrorMessage(result.expected, result.actual);
    return interactive ? `${errMsg} [Compare Snapshots](#${id})` : errMsg;
  }

  const suffix = interactive ? ` ([Compare Snapshots](#${id}))` : '';
  if (result.result === 'fail') {
    return diffThresholdErrorMessage(result.diff.diffPixelPercentage) + suffix;
  }

  return interactive ? `[Snapshot Matches](#${id})` : 'snapshot matched';
}

function logMessage(result: SnapshotMatchResult) {
  const isInteractive = Cypress.config('isInteractive');

  const {
    subject,
    taskData: { titlePath: path, maxDiffPercentage },
    result: reason,
  } = result;
  // TODO: Add additional check or use URL here to avoid unexpected edge-cases & make valid ID
  const id = `cypress-snapshot-verify-result-${path.join('-')}`.trimEnd().replaceAll(/\s+/g, '_');
  const hasDiff = reason === 'fail' || reason === 'pass';
  const interactiveResult = hasDiff || reason === 'size';
  const success = reason === 'pass' || reason === 'created' || reason === 'updated';

  // Only add elements when in Cypress `open` mode
  if (isInteractive && interactiveResult) {
    const { diff, expected, actual } = result;
    const expectedData = JSON.stringify(expected);
    const actualData = JSON.stringify(actual);
    const diffData = JSON.stringify(diff);
    const testPath = JSON.stringify(path);

    if (!top) throw new Error('Failed to access root document to append snapshot element');

    // Insert element or update element attributes to store data about screenshot/snapshot/diff
    const dataDiv = top.document.getElementById(id);
    if (!dataDiv) {
      Cypress.$(top.document.body).append(`<div id="${id}"
                  data-expected='${expectedData}'
                  data-actual='${actualData}'
                  data-diff='${diffData}'
                  data-test-path='${testPath}'
                />`);
    } else {
      dataDiv.setAttribute('data-expected', expectedData);
      dataDiv.setAttribute('data-actual', actualData);
      dataDiv.setAttribute('data-diff', diffData);
      dataDiv.setAttribute('data-test-path', testPath);
    }
  }

  const log = Cypress.log({
    $el: subject,
    name: COMMAND_NAME,
    displayName: 'snapshot',
    message: generateLogMessage(result, id, isInteractive),
    consoleProps: () => result,
    autoEnd: false,
    type: 'child',
  });

  // Signal in Cypress log the element that was screenshot for our snapshot (lawdy that's a lot of shots)
  log.snapshot();

  // The remaining reasons are either an actual pass or an autopass condition (updated/created)
  if (success) {
    // TODO: Figure out how to set custom CSS/color for snapshot command name
    log.set('state', 'success');
    log.finish();
    log.end();
    return subject;
  }

  let diffErr = '';
  if (reason === 'fail') {
    diffErr = diffThresholdErrorMessage(result.diff.diffPixelPercentage, maxDiffPercentage);
  } else if (reason === 'size') {
    // NOTE: the above if statement should technically be able to be an else but the type system is failing to recognize it
    diffErr = mismatchSizeErrorMessage(result.expected, result.actual);
  }
  const err = new Error(diffErr);
  log.error(err);
  log.end();
  throw err;
}

export { logMessage };
