import { PLUGIN_CONFIG_ENV_KEY, SnapshotVerifyConfig, initConfig } from '../config';
import { COMMAND_NAME, NO_LOG } from '../constants';
import { initUi } from '../ui';
import { matchesSnapshot } from './snapshot';

interface SnapshotOptions {
  snapshotsFolder?: string;
  maxDiffPercentage?: number;
  screenshotConfig?: Partial<Cypress.ScreenshotOptions>;
}

declare global {
  namespace Cypress {
    interface Chainable {
      /**
       * Takes a screenshot of the application under test and confirms it matches the expected snapshot.
       * @param [filename] filename for the resulting snapshot and screenshot
       * @param [options] optional configuration options for command
       * @param [options.maxDiffPercentage=0.0] maximum percentage difference allowed between
       * @param [options.snapshotsFolder=./cypress/snapshots] directory to store the snapshot in, relative to where Cypress is run from
       * @param [options.screenshotConfig] options to pass to the cypress screenshot command
       * @example cy.matchesSnapshot();
       *  cy.get('.App').matchesSnapshot({ filename: 'app-root', maxDiffPercentage: 0.1, screenshotConfig: { clip: { x: 0, y: 0, width: 100, height: 100 } } });
       */
      [COMMAND_NAME]: (filename?: string, options?: SnapshotOptions) => Chainable<JQuery<HTMLElement>>;
    }
  }

  interface Window {
    closeSnapshotDialog: VoidFunction;
    acceptSnapshotChange: VoidFunction;
  }
}

/**
 * Register visual diff UI elements and add global `matchesSnapshot` command
 */
function addMatchSnapshotCommand() {
  // Registers UI elements for later diffing before each spec if in interactive session
  if (Cypress.config('isInteractive')) {
    before(() => {
      initUi();
    });
  }

  // Retrieve snapshot details from config before registering command
  const envConfig: Partial<SnapshotVerifyConfig> =
    (Cypress.env()[PLUGIN_CONFIG_ENV_KEY] as Partial<SnapshotVerifyConfig>) || {};
  const { maxDiffPercentage: maxDiff, snapshotsFolder: cfgSnapshotsFolder } = initConfig(envConfig);

  Cypress.Commands.add(
    COMMAND_NAME,
    { prevSubject: ['optional'] },
    (
      cmdSubject: JQuery | void,
      filename,
      { snapshotsFolder = cfgSnapshotsFolder, maxDiffPercentage = maxDiff, screenshotConfig }: SnapshotOptions = {},
    ) => {
      // This is a dual command so either wrap the subject or target the entire document
      const target = cmdSubject ? cy.wrap(cmdSubject, NO_LOG) : cy.document(NO_LOG).then((doc) => doc.body);

      target.then((subject) =>
        matchesSnapshot(subject, snapshotsFolder, maxDiffPercentage, filename, screenshotConfig),
      );
      return target;
    },
  );
}

export { addMatchSnapshotCommand };
