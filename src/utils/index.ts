// Copied verbatim from Cypress types because no explicit type is exported
export interface CypressScreenshotProps {
  path: string;
  size: number;
  dimensions: { width: number; height: number };
  multipart: boolean;
  pixelRatio: number;
  takenAt: string;
  name: string;
  blackout: string[];
  duration: number;
  testAttemptIndex: number;
}

export interface TaskData {
  spec: Cypress.Spec;
  isHeadless: boolean;
  title: string;
  titlePath: string[];
  snapshotTitle: string;
  snapshotDetails?: CypressScreenshotProps;
  snapshotFolder: string;
  maxDiffPercentage: number;
}

export interface TestOptions {
  title?: string;
  maxDiffPercentage?: number;
}

interface TestTitles {
  title: string;
  titlePath: string[];
}

export interface PathWithDir {
  dir: string;
  fullPath: string;
}

function getTest(): TestTitles {
  if (Cypress === undefined) {
    throw new Error('Only use within Cypress');
  }

  return Cypress.currentTest;
}

function getTaskData(folder: string, customTitle?: string, maxDiffPercentage = 0.0): TaskData {
  const { title, titlePath } = getTest();

  return {
    spec: Cypress.spec,
    isHeadless: Cypress.browser.isHeadless,
    snapshotTitle: customTitle || titlePath.join(' -- '),
    snapshotFolder: folder,
    title,
    titlePath,
    maxDiffPercentage,
  };
}

export { getTaskData };
