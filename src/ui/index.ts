import { MODAL_ELEM_ID, MODAL_ROOT_SELECTOR, NO_LOG, TASK_LOAD_FILE } from '../constants';
import { CSS_PATH, MODAL_PATH, SCRIPT_PATH } from '../tasks/getFile';

function initUi(serverEnabled = true, serverPort = 4042, serverHost = 'localhost') {
  if (top === null) throw new Error('Unable to access root window to initialize UI');
  const $head = top.document.head;
  const $body = top.document.body;

  if ($head.querySelector(`${MODAL_ROOT_SELECTOR}-script`) === null) {
    cy.task<string>(TASK_LOAD_FILE, SCRIPT_PATH, NO_LOG).then((content) => {
      content = content
        .replace("const HOST = 'localhost';", `const HOST = '${serverHost}';`)
        .replace('const PORT = 4042;', `const PORT = ${serverPort};`)
        .replace('const UPDATE_ENABLED = true;', `const UPDATE_ENABLED = ${serverEnabled ? 'true' : 'false'};`);
      cy.$$(`<script id="${MODAL_ELEM_ID}-script">${content}</script>`).appendTo($head);
    });
  }
  if ($head.querySelector(`${MODAL_ROOT_SELECTOR}-style`) === null) {
    cy.task<string>(TASK_LOAD_FILE, CSS_PATH, NO_LOG).then((content) => {
      cy.$$(`<style type="text/css" id="${MODAL_ELEM_ID}-style">${content}</style>`).appendTo($head);
    });
  }
  if ($body.querySelector(`${MODAL_ROOT_SELECTOR}-root`) === null) {
    cy.task<string>(TASK_LOAD_FILE, MODAL_PATH, NO_LOG).then((content) => {
      cy.$$(`<div id="${MODAL_ELEM_ID}-root"></div>`).append(content).appendTo($body);
    });
  }
}

export { initUi };
