const { rmdir } = require('fs');
const { defineConfig } = require('cypress');
const { initPlugin } = require('.');

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      initPlugin(on, config);

      on('task', {
        rmdir(path) {
          return new Promise((resolve) => {
            rmdir(path, { recursive: true }, (err) => {
              if (err) {
                console.error(err);
              }
              resolve(null);
            });
          });
        },
      });

      return config;
    },
    trashAssetsBeforeRuns: true,
    video: false,
  },
});
