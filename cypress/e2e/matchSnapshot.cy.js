/// <reference types="cypress"/>

describe('matchSnapshot', () => {
  const screenshotsFolder = Cypress.browser.isHeadless
    ? `./cypress/screenshots/${Cypress.spec.name}`
    : './cypress/screenshots';
  const snapshotsFolder = Cypress.browser.isHeadless
    ? `./cypress/snapshots/${Cypress.spec.name}`
    : './cypress/snapshots';

  beforeEach(() => {
    cy.visit('cypress/pages/index.html');
  });

  describe('no options', () => {
    it('sets base snapshot with generic name', () => {
      cy.matchesSnapshot();
      cy.readFile(`${screenshotsFolder}/matchSnapshot -- no options -- sets base snapshot with generic name.png`);
      cy.readFile(`${snapshotsFolder}/matchSnapshot -- no options -- sets base snapshot with generic name.png`);
    });
  });

  describe('with custom name', () => {
    it('sets base snapshot', () => {
      cy.matchesSnapshot('test snapshot');
      cy.readFile(`${screenshotsFolder}/test snapshot.png`);
      cy.readFile(`${snapshotsFolder}/test snapshot.png`);
    });

    it('fails with differing screenshot', () => {
      // This allows us to expect matchesSnapshot to fail
      Cypress.on('fail', (error) => {
        if (!error.message.includes('crossed diff threshold')) throw error;
      });
      // Confirm base snapshot is still there
      cy.readFile(`${screenshotsFolder}/test snapshot.png`);

      // Visit secondary page to simulate change in page - base already exists this time because we're setting it
      cy.visit('cypress/pages/secondary.html');

      // This should throw an error but should be caught
      cy.matchesSnapshot('test snapshot');
    });
  });

  describe('with custom snapshot dir', () => {
    const testDir = './cypress/test-snapshots';
    const snapshotsFolder = Cypress.browser.isHeadless ? `${testDir}/${Cypress.spec.name}` : testDir;

    it('sets base snapshot in specified dir', () => {
      cy.matchesSnapshot('test dir snapshot', { snapshotsFolder: testDir });
      cy.readFile(`${snapshotsFolder}/test dir snapshot.png`);
    });

    it('fails with differing screenshot', () => {
      // This allows us to expect matchesSnapshot to fail
      Cypress.on('fail', (error) => {
        if (!error.message.includes('crossed diff threshold')) throw error;
      });
      // Confirm base snapshot is still there
      cy.readFile(`${screenshotsFolder}/test dir snapshot.png`);

      // Visit secondary page to simulate change in page - base already exists this time because we're setting it
      cy.visit('cypress/pages/secondary.html');

      // This should throw an error but should be caught
      cy.matchesSnapshot('test dir snapshot', { snapshotsFolder: testDir });
    });
  });
});
