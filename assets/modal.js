const HOST = 'localhost';
const PORT = 4042;
const UPDATE_ENABLED = true;
let updateBaseSnapshot = undefined;

function closeSnapshotDialog() {
  Cypress.$('#cypress-snapshot-verify-modal-root', top.document).css('visibility', 'hidden');
}

function sendUpdateBaseSnapshotRequest(expectedPath, actualPath) {
  if (actualPath === undefined || actualPath === '') {
    console.error('Actual screenshot path is invalid, unable to accept');
    return;
  }
  if (expectedPath === undefined || expectedPath === '') {
    console.error('Expected screenshot path is invalid, unable to accept');
    return;
  }

  Cypress.$('button#ignore', top.document).prop('disabled', true);
  Cypress.$('button#update', top.document).prop('disabled', true);
  fetch(`http://${HOST}:${PORT}`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ expected: expectedPath, actual: actualPath }),
  })
    .then(async (res) => {
      // TODO: Convert this to a ribbon or alert of some kind
      console.log('Update request succeeded!', res, await res.json());
      closeSnapshotDialog();
    })
    .catch((err) => console.error('Error submitting update request:', err));
}

(() => {
  Cypress.$(document.body).on('click', 'a[href^="#cypress-snapshot-verify-result-"]', (e) => {
    e.preventDefault();
    e.stopPropagation();

    let success = false;
    const {
      currentTarget: { hash: targetId },
    } = e;
    const target = document.querySelector(`${targetId}[data-expected][data-actual]`);
    if (!target) {
      console.error('Failed to find target link');
      return;
    }

    try {
      const testPath = JSON.parse(target.dataset.testPath);
      const expected = JSON.parse(target.dataset.expected);
      const actualData = JSON.parse(target.dataset.actual);
      const diff = JSON.parse(target.dataset.diff);
      success = diff.diffPixelPercentage === 0;

      document.querySelector('img#expected-snapshot').setAttribute('src', `data:image/png;base64,${expected.b64}`);
      document.querySelector(
        '#expected > header > span.snapshot-dimensions',
      ).textContent = `${expected.width}x${expected.height}`;
      document.querySelector('img#actual-snapshot').setAttribute('src', `data:image/png;base64,${actualData.b64}`);
      document.querySelector(
        '#actual > header > span.snapshot-dimensions',
      ).textContent = `${actualData.width}x${actualData.height}`;
      document.querySelector('#diff-percentage').textContent = diff.diffPixelPercentage?.toFixed(2) ?? 'NA';
      const badge = document.querySelector('#test-status-badge');
      if (success) {
        badge.textContent = 'success';
        badge.classList.add('success');
      } else {
        badge.textContent = 'failed';
        badge.classList.remove('success');
      }
      document.querySelector('#cypress-snapshot-verify-modal #test-path').replaceChildren(
        ...testPath.map((title) => {
          const segment = document.createElement('span');
          segment.textContent = title;
          return segment;
        }),
      );

      // Bind updateBaseScreenshot function with arguments
      updateBaseSnapshot = () => sendUpdateBaseSnapshotRequest(expected.path, actualData.path);

      // Update button will be disabled if the test was successful or if the server is disabled
      const updateBtn = Cypress.$('button#update', top.document);
      updateBtn.prop('title', 'Update snapshot');
      updateBtn.prop('disabled', success || !UPDATE_ENABLED);
      if (!UPDATE_ENABLED) {
        updateBtn.prop('title', 'Updating of snapshots is disabled');
      }
    } catch (ex) {
      const headingElem = document.createElement('h2');
      headingElem.textContent = 'Failed to parse dataset';
      const errMsgElem = document.createElement('p');
      errMsgElem.textContent = ex.message;

      document
        .querySelector('#cypress-snapshot-verify-modal #test-path')
        ?.replaceChildren('Verify Screenshot - Failed');
      document.querySelector('#cypress-snapshot-verify-modal #test-diff')?.classList.add('parse-error');
      document.querySelector('#cypress-snapshot-verify-modal #test-diff')?.replaceChildren(headingElem, errMsgElem);

      const updateBtn = Cypress.$('button#update', top.document);
      updateBtn.prop('disabled', true);
    }

    // Show UI
    Cypress.$('#cypress-snapshot-verify-modal-root', top.document).css('visibility', 'visible');
  });
})();
