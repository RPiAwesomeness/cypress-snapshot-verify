# Cypress Snapshot Verify

A Cypress plugin that assists with visual regression testing by providing automatic and manual snapshot verification.

The term **snapshot** is used to represent the expected screenshot which all future runs will be compared against

[![NPM](https://nodei.co/npm/cypress-snapshot-verify.png?compact=true)](https://nodei.co/npm/cypress-snapshot-verify/)

## Installation

`npm install --save-dev cypress-snapshot-verify`

## Usage

```ts
describe('App Snapshots', () => {
  it('base snapshot with generic name', () => {
    cy.visit('/');
    cy.get('.App').should('be.visible').matchesSnapshot();
  });

  it('snapshot with specific name and higher tolerance for diff', () => {
    cy.visit('/');
    cy.get('.App').should('be.visible').matchesSnapshot('screenshot', { maxDiffPercentage: 0.2 });
  });
});
```

## Configuration

This plugin supports the following configuration values, set via the `cypress-snapshot-verify` env key in the cypress config. Example `cypress.config.js`:

```js
const { defineConfig } = require('cypress');
const { initPlugin } = require('cypress-snapshot-verify');

module.exports = defineConfig({
  env: {
    'cypress-snapshot-verify': {
      snapshotsFolder: '../docs/snapshots/',
    },
  },
  e2e: {
    setupNodeEvents(on, config) {
      initPlugin(on, config);
      return config;
    },
  },
});
```

Also add the following lines to the `support/e2e.js` file:

```js
import { addMatchSnapshotCommand } from 'cypress-snapshot-verify';

addMatchSnapshotCommand();
```

### Plugin Configuration

- `passNewSnapshots` - `boolean`

  Default: `false`

  Automatically create the snapshot and pass the test if the snapshot doesn't exist. If `false` the test will fail with an error if the snapshot doesn't exist.

- `updateFailingSnapshots` - `boolean`

  Default: `false`

  Automatically update a snapshot that would normally fail due to size change or % difference and pass the test

- `errorOnMatchingSnapshotNames` - `boolean`

  Default: `true`

  Throw an error if a snapshot has the same name as an already-existing one. Setting this to `false` will overwrite the existing snapshot file.

- `snapshotsFolder` - `string`

  Default: `./cypress/snapshots`

  The directory you want the snapshots to be placed in

- `maxDiffPercentage` - `number`

  Default: `0.0`

  Maximum diff percentage calculated between snapshot and actual before the test fails

- `serverEnabled` - `boolean`

  Default: `true`

  Enables a server for the plugin and the ability to update failing snapshots from a modal within the Cypress UI

- `serverHost` - `string`

  Default: `localhost`

  Server hostname for plugin's server to attempt to bind to

- `serverPort`: `number`

  Default: `2121`

  Server port for plugin's server to attempt to bind to

## Testing

Testing is done via Cypress tests defined in the `cypress` folder. Build the library first then run the Cypress tests.

```
npm run build
npm run test
```
